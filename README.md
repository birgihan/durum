**26.06.2016’dan bugüne (3 Ay 11 Gün.)**

1-	Kullanıcı kayıt formunu oluştururken formun daha estetik durması amacıyla smartwizard kullandım.

**SEBEBİ** : Öncelikli amacım formda çok fazla alan olduğu için daha estetik bir görünüşe sahip olması. 

2-	Formlarda ve güncellemelerde ajax kullandım.

**SEBEBİ** : Smartwizard formları ajax ile gönderiyordu. Bu sayede ajax ile tanıştım. Daha sonralarda ise ajaxta serialize metodu ile daha kısa bir şekilde formdan verileri almayı öğrendim. Serializeden önce ajax tarafında daha uzun işlemler yapıyordum. Form inputlarında EmployeeData[inputname] gibi uzun şekilde yazıyordum. Serialize ile sadece form isimlerini kullanarak verileri almayı gördüm.

3-	Formda kullanıcı 4 tabloya bilgi kaydediyorum. Bilgileri kaydederken, bilgilerin kimin olduğunu anlayabilmek için user tablosundaki id ile diğer tablolardaki user_id arasında tablolarda relation kurdum. 

4- **Temel olarak CRUD için gerekli tüm fonksiyonları tek bir satırla oluşturmak için controller oluştururken --resource** yazmayı index'in listeleme için, create'in oluşturma için, store'un veritabanına yazılmasını kontrol etmek için
	update() 'in düzenlenecek sayfa işlemlerini yapması için destroy'un silme işlemlerini yapması için kullanıldığını öğrendim.
	
5- **Authentication** Yaptığım proje de php artisan make:auth kullanarak kimlik doğrulaması yapmayı öğrendim. Bununla beraber middleware ile admin ve kullanıcı kısıtlamarını yaptım. Route'da middleware tanımlayı gördüm.

6- **Mailtrap** Kullanıcıların **_şifremi unuttum_** kısmından şifrelerini değiştirmeleri için mailtrap kullanarak eposta göndermesini yaptım. Şifre yenileme e postası ile şifre resetleme yapıldı.